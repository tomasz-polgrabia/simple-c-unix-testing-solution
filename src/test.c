#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define BLANK      0
#define SIMPLE_ARG 1
#define COM_ARG    2

int split_args(char *cmdline) {
    int n = strlen(cmdline);
    int state = BLANK;
    int args = 0;

    for (int i = 0; i < n; i++) {
        switch(state) {
            case BLANK:
                if (cmdline[i] <= 0x20) { // control chars including space
                    cmdline[i] = '\0';
                } else if (cmdline[i] == '\"') {
                    cmdline[i] = '\0';
                    state = COM_ARG;
                    ++args;
                } else {
                    state = SIMPLE_ARG;
                    ++args;
                }
                break;
            case SIMPLE_ARG:
                if (cmdline[i] <= 0x20) {
                    cmdline[i] = '\0';
                    state = BLANK;
                }
                break;
            case COM_ARG:
                if (cmdline[i] == '"') {
                    if (i == 0) {
                        // IMPOSSIBLE STATE
                        fprintf (stderr,
                                "Impossible state closing char"
                                " on the beginning");
                        return -1;
                    } else {
                        if (cmdline[i-1] != '\\') {
                            cmdline[i] = '\0';
                            state = BLANK;
                        }
                    }
                }
                break;
            default:
                fprintf(stderr, "I don't know this state. Probably bug :)\n");
                return -1;
        }
    }
    return args;
}

int fill_args(char **args, int argc, char *cmdline) {

    while (*cmdline == '\0') {
        ++cmdline;
    }

    for (int i = 0; i < argc; i++) {
        args[i] = cmdline;
        int m = strlen(cmdline);
        cmdline = cmdline + m;

        while (*cmdline == '\0') {
            ++cmdline;
        }

    }
    return 0;
}

int help() {
    fprintf (stderr, "tester inputfile outputfile testcmdline\n");
    return 0;
}

int main(int argc, char **argv) {
    printf ("Tester 0.0.1\n");

    if (argc != 4) {
        fprintf (stderr, "Invalid usage");
        help();
        return 1;
    }

    int din = open(argv[1], O_RDONLY);
    if (din < 0) {
        perror("Cannot read input");
        return 2;
    }

    int dout = open(argv[2], O_CREAT | O_WRONLY, 0755);
    if (dout < 0) {
        perror("Cannot open output");
        return 2;
    }

    pid_t p = fork();
    if (p < 0) {
        perror("Cannot fork");
        return 2;
    }

    if (p == 0) {
        fprintf (stderr, "I'm the child\n");

        int res = -1;
        int cmd_args_nr = split_args(argv[3]);
        printf("Split args: %d\n", cmd_args_nr);
        if (cmd_args_nr < 0) {
            fprintf (stderr, "Splitting args error\n");
            return 3;
        }
        char **args = (char **)malloc(sizeof(char*)*(cmd_args_nr+1));
        args[cmd_args_nr] = NULL;
        // freeing args array should be child process responsibility
        // we have no control after execve
        fill_args(args, cmd_args_nr, argv[3]);


        // FIXME currently without args
        if (close(0) < 0) {
            perror("Cannot close STDIN");
            return 2;
        }

        if (close(1) < 0) {
            perror("Cannot close STDOUT");
            return 2;
        }

        if (dup(din) < 0) {
            perror("Cannot bind the output file.txt as STDIN");
            return 2;
        }

        if (dup(dout) < 0) {
            perror("Cannot bind the input file.txt as STDOUT");
            return 2;
        }

        if ((res = execvpe(args[0], args, NULL)) < 0) {
            perror("execve A");
            return 1;
        }
        return res;
    }

    if (waitpid(p, NULL, 0) < 0) {
        perror("Cannot wait for child process");
        return 2;
    }

    if (close(din) < 0) {
        perror("Cannot close input file");
        return 2;
    }

    if (close(dout) < 0) {
        perror("Cannot close output file");
        return 2;
    }



    return 0;
}
