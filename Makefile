SOURCES=$(wildcard src/*.c)
BINARIES=$(SOURCES:src/%.c=bin/%.c.o)
FLAGS=-Wall -Wextra -pedantic -g
LFLAGS=$(FLAGS)
CFLAGS=$(FLAGS)


all: program

program: $(BINARIES)
	$(CXX) $(LFLAGS) -o $@ $^

bin/%.c.o: src/%.c bin
	$(CXX) $(CFLAGS) -c -o $@ $<

bin:
	mkdir -p bin

.PHONY: clean

clean:
	rm -rf *.o program bin
